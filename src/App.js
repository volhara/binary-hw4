import Chat from "./app/components/Chat";

function App() {
  return (
    <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json"></Chat>
  );
}

export default App;
