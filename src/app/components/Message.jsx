import React from "react";
import { getStringTime } from "../helpers/DateHelper";

export class Message extends React.Component {
  constructor(props) {
    super(props);

    this.onLikeClick = this.onLikeClick.bind(this);
  }

  onLikeClick(e) {
    this.props.OnLikeClick(this.props.Id);
  }

  render() {
    return (
      <div className="message">
        <div className="message-info">
          <img
            className="message-user-avatar"
            src={this.props.MessageUserAvatar}
            alt={this.props.MessageUserName}
          ></img>
          <div>
            <p className="message-user-name">{this.props.MessageUserName}</p>
            <span className="message-text">{this.props.MessageText}</span>
          </div>
        </div>
        <div className="message-controls">
          <svg
            onClick={this.onLikeClick}
            xmlns="http://www.w3.org/2000/svg"
            stroke="red"
            width="40"
            height="40"
            className={this.props.IsLiked ? "message-liked" : "message-like"}
            viewBox="0 -2 18 22"
          >
            <path
              fillRule="evenodd"
              d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
            />
          </svg>
          <span className="message-time">
            {getStringTime(new Date(Date.parse(this.props.MessageTime)))}
          </span>
        </div>
      </div>
    );
  }
}
