import React from "react";
import { Header } from "./Header";
import { MessageList } from "./MessageList";
import { MessageInput } from "./MessageInput";
import { Preloader } from "./Preloader";
import { getUuid } from "../helpers/uuidHelper";

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      isNewMessage: true,
      inputMode: "New",
      messageForEdit: {},
    };

    this.onLikeClick = this.onLikeClick.bind(this);
    this.onSendMessage = this.onSendMessage.bind(this);
    this.onDeleteMessageClick = this.onDeleteMessageClick.bind(this);
    this.onEditMessageClick = this.onEditMessageClick.bind(this);
    this.onEditMessage = this.onEditMessage.bind(this);
  }

  async componentDidMount() {
    this.setState({ messageList: await this.getMessageList(this.props.url) });
    this.setState({ isLoaded: true });
  }

  async getMessageList(url) {
    let response = await fetch(url);
    return await response.json();
  }

  getUserCount(messageList) {
    const map = new Map();

    for (const item of messageList) {
      map.set(item.userId, 1);
    }

    return map.size;
  }

  getMessageIndexById(id) {
    return this.state.messageList.findIndex((item) => {
      return item.id === id;
    });
  }

  onLikeClick(id) {
    let index = this.getMessageIndexById(id);
    let newMessageList = this.state.messageList.slice();
    newMessageList[index]["isLiked"] = !newMessageList[index]["isLiked"];
    this.setState({ messageList: newMessageList, isNewMessage: false });
  }

  onSendMessage(messageText) {
    this.state.messageList.push({
      id: getUuid(),
      userId: "testUser",
      text: messageText,
      createdAt: new Date(),
      isLocal: true,
    });
    this.setState({ messageList: this.state.messageList, isNewMessage: true });
  }

  onDeleteMessageClick(id) {
    this.setState({
      inputMode: "New",
    });
    let index = this.getMessageIndexById(id);
    let newMessageList = [
      ...this.state.messageList.slice(0, index),
      ...this.state.messageList.slice(index + 1),
    ];
    this.setState({ messageList: newMessageList, isNewMessage: false });
  }

  onEditMessageClick(id) {
    let index = this.getMessageIndexById(id);
    let messageForEdit = this.state.messageList[index];
    this.setState({
      inputMode: "Edit",
      messageForEdit: { id: messageForEdit.id, text: messageForEdit.text },
    });
  }

  onEditMessage(editedMessageId, editedMessageText) {
    this.setState({
      inputMode: "New",
    });
    let index = this.getMessageIndexById(editedMessageId);
    let newMessageList = this.state.messageList.slice();
    newMessageList[index]["text"] = editedMessageText;
    this.setState({ messageList: newMessageList, isNewMessage: false });
  }

  render() {
    if (this.state.isLoaded) {
      return (
        <div className="chat">
          <Header
            Title="MyChat"
            UserCount={this.getUserCount(this.state.messageList)}
            MessageCount={this.state.messageList.length}
            LastMessageDate={
              this.state.messageList[this.state.messageList.length - 1]
                .createdAt
            }
          />
          <MessageList
            MessageList={this.state.messageList}
            IsNewMessage={this.state.isNewMessage}
            OnLikeClick={this.onLikeClick}
            OnDeleteMessageClick={this.onDeleteMessageClick}
            OnEditMessageClick={this.onEditMessageClick}
          />
          <MessageInput
            Mode={this.state.inputMode}
            MessageForEdit={this.state.messageForEdit}
            OnSendMessage={this.onSendMessage}
            OnEditMessage={this.onEditMessage}
          />
        </div>
      );
    } else {
      return <Preloader />;
    }
  }
}

export default Chat;
