import React from "react";
import { Message } from "./Message";
import { OwnMessage } from "./OwnMessage";
import { getDiffDate, getFormatedDate } from "../helpers/DateHelper";
import { getUuid } from "../helpers/uuidHelper";

export class MessageList extends React.Component {
  // constructor(props) {
  //   super(props);
  // }

  scrollToBottom() {
    if (this.props.IsNewMessage) {
      this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate(prevProps, prevState) {
    this.scrollToBottom();
  }

  *renderMessages() {
    let lastDate = -1;
    for (const item of this.props.MessageList) {
      let date = new Date(Date.parse(item.createdAt));
      if (lastDate !== date.getDate()) {
        let countDays = getDiffDate(date);
        let dateInfo = "";
        if (countDays === 1) {
          dateInfo = "Today";
        } else if (countDays === 2) {
          dateInfo = "Yesterday";
        } else {
          dateInfo = getFormatedDate(date);
        }
        lastDate = date.getDate();
        yield (
          <div className="messages-divider" key={getUuid()}>
            <hr className="divider" />
            <div className="messages-divider-date">
              <span>{dateInfo}</span>
            </div>
          </div>
        );
      }
      if (item.isLocal) {
        yield (
          <OwnMessage
            MessageText={item.text}
            MessageTime={item.createdAt}
            key={item.id}
            Id={item.id}
            OnDeleteClick={this.props.OnDeleteMessageClick}
            OnEditClick={this.props.OnEditMessageClick}
          />
        );
      } else {
        yield (
          <Message
            MessageUserAvatar={item.avatar}
            MessageUserName={item.user}
            MessageText={item.text}
            MessageTime={item.createdAt}
            IsLiked={item.isLiked}
            key={item.id}
            Id={item.id}
            OnLikeClick={this.props.OnLikeClick}
          />
        );
      }
    }
  }

  render() {
    return (
      <div className="message-list">
        {[...this.renderMessages()]}
        <div
          ref={(el) => {
            this.messagesEnd = el;
          }}
        />
      </div>
    );
  }
}
