import React from "react";

export class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = { value: "" };

    this.onChangeInputText = this.onChangeInputText.bind(this);
    this.onSendMessageClick = this.onSendMessageClick.bind(this);
    this.onEditMessageClick = this.onEditMessageClick.bind(this);
  }

  onSendMessageClick(e) {
    this.props.OnSendMessage(this.inputText.value);
    this.cleatInput();
  }

  onEditMessageClick(e) {
    this.props.OnEditMessage(
      this.props.MessageForEdit.id,
      this.inputText.value
    );
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.Mode === "Edit" && nextProps.Mode !== this.props.Mode) {
      this.setState({ value: nextProps.MessageForEdit.text });
      this.inputText.focus();
      return true;
    } else if (nextProps.Mode === "New" && nextProps.Mode !== this.props.Mode) {
      this.setState({ value: "" });
      return true;
    } else if (nextState.value !== this.state.value) {
      return true;
    } else {
      return false;
    }
  }

  cleatInput() {
    this.setState({ value: "" });
  }

  onChangeInputText(event) {
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <div className="message-input">
        <input
          className="message-input-text"
          type="text"
          ref={(el) => (this.inputText = el)}
          value={this.state.value}
          onChange={this.onChangeInputText}
        />
        {this.props.Mode === "New" ? (
          <button
            className="message-input-button"
            onClick={this.onSendMessageClick}
          >
            Send
          </button>
        ) : (
          <button
            className="message-input-button"
            onClick={this.onEditMessageClick}
          >
            Edit
          </button>
        )}
      </div>
    );
  }
}
