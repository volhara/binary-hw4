import React from "react";
import { getDateStringWithTime } from "../helpers/DateHelper";

export class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <p className="header-title">{this.props.Title}</p>
        <p className="header-users-count">{this.props.UserCount} users</p>
        <p className="header-messages-count">{this.props.MessageCount}</p>

        <p className="header-last-message-date">
          Last message date:
          {getDateStringWithTime(
            new Date(Date.parse(this.props.LastMessageDate))
          )}
        </p>
      </div>
    );
  }
}
